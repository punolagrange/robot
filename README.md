## Assignment 1

Requirements: python3 and pipenv

To run the bot in stdout:
```sh
cd be/core
pipenv install
make init
```

To run tests:
```sh
cd be/core
pipenv install
make test
```

## Assignment 2

- Piggy backs on the first assignment.
- The core functionalities of the robot are exposed via simple lambda functions 
served by an AWS Gateway. A dynamodb key pair is used to persist the state of the robot (x, y, direction).
- Web responsive app: https://bitbucket.org/punolagrange/robot-app/src/master/
- URL: https://sourcedrobot.go-demo.bambu.life/

## Notes
- BE and FE resources are fully created using CDK. (infra folder)
- BE is fully tested with high test coverage.
- Fully automated CI/CD pipelines have been created for both BE and FE.
- The FE is a basic React app that only uses vanilla React state for state management.
- Ideally, hosting the FE statics files in S3 served by CloudFront would be the ideal solution.
Here, the FE static files are served by nginx that runs as an ECS service.



