import unittest
from persist import parse_dynamo_item_key, parse_dynamo_item, attribute_update


class GridTest(unittest.TestCase):

    def test_parse_dynamo_item_key(self):
        self.assertEqual(
            parse_dynamo_item_key({"N": "1"}),
            1
        )
        self.assertEqual(
            parse_dynamo_item_key({"S": "a"}),
            "a"
        )
        self.assertEqual(
            parse_dynamo_item_key({"NULL": True}),
            None
        )

    def test_parse_dynamo_item(self):
        self.assertEqual(parse_dynamo_item({'y': {'N': '1'}, 'id': {'N': '0'}, 'x': {
            'N': '0'}, 'direction': {'S': 'NORTH'}}), (0, 1, "NORTH"))

    def test_attribute_update(self):
        self.assertEqual(
            attribute_update(1),
            {"Value": {"N": "1"}, "Action": "PUT"}
        )
        self.assertEqual(
            attribute_update("NORTH"),
            {"Value": {"S": "NORTH"}, "Action": "PUT"}
        )
        self.assertEqual(
            attribute_update(None),
            {"Value": {"NULL": True}, "Action": "PUT"}
        )


if __name__ == '__main__':
    unittest.main()
