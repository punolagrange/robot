import boto3
from core.grid import Grid
from core.config import GRID_SIZE


def parse_dynamo_item_key(dynamo_item_key):
    """
    dynamo_item_key:
        {'N': '1'}
    """
    if "N" in dynamo_item_key:
        return None if dynamo_item_key == {
            "NULL": True} else int(dynamo_item_key["N"])
    return None if dynamo_item_key == {"NULL": True} else dynamo_item_key["S"]


def parse_dynamo_item(dynamo_item):
    """
    dynamo_item:
        {'y': {'N': '1'}, 'id': {'N': '0'}, 'x': {'N': '0'}, 'direction': {'S': 'NORTH'}}
    """
    x = parse_dynamo_item_key(dynamo_item["x"])
    y = parse_dynamo_item_key(dynamo_item["y"])
    direction = parse_dynamo_item_key(dynamo_item["direction"])
    return x, y, direction


def attribute_update(x: int or str or None):
    if isinstance(x, int):
        return {"Value": {"N": "{0}".format(x)}, "Action": "PUT"}
    elif isinstance(x, str):
        return {"Value": {"S": x}, "Action": "PUT"}
    elif x is None:
        return {"Value": {"NULL": True}, "Action": "PUT"}


########################################################
#
# THE BELOW FUNCTIONS PRODUCE SIDE EFFECTS
#
########################################################
def read_dynamo_key(table_name: str):
    dynamodb = boto3.client('dynamodb')
    res = dynamodb.get_item(
        TableName=table_name,
        Key={
            "id": {
                "N": "0",
            }
        }
    )
    return res


def write_to_dynamo_key(
        table_name,
        x: int or None,
        y: int or None,
        direction: str or None) -> None:
    dynamodb = boto3.client('dynamodb')
    dynamodb.update_item(
        TableName=table_name,
        Key={
            "id": {
                "N": "0",
            }
        },
        AttributeUpdates={
            "x": attribute_update(x),
            "y": attribute_update(y),
            "direction": attribute_update(direction),
        }
    )


def read_from_state(table_name: str, skip_placed_check=False) -> Grid:
    x, y, direction = parse_dynamo_item(read_dynamo_key(table_name)["Item"])
    grid = Grid(GRID_SIZE, x, y, direction)

    if skip_placed_check:
        return True, None, grid

    if not grid.is_placed():
        return False, "Robot is not on the grid!", None
    return True, None, grid


def persist_state(table_name: str, grid: Grid) -> None:
    write_to_dynamo_key(table_name, grid.x, grid.y, grid.direction)
