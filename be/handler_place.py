import json
import os
from operator import itemgetter
from core.move import valid_coordinates
from core.grid import Grid
from core.config import GRID_SIZE
from persist import persist_state


def handler(event, _context):
    table_name = os.environ["table_name"]
    # validate user input
    body = json.loads(event["body"])
    if "x" not in body or "y" not in body or "direction" not in body:
        return {
            "statusCode": 400,
            "headers": {
                "Access-Control-Allow-Headers": "*",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": "*",
            },
            "body": "invalid inputs!"
        }

    x, y, direction = itemgetter('x', 'y', 'direction')(body)
    x = int(x)
    y = int(y)

    # validate coordinates
    is_valid_coordinates = valid_coordinates(x, y)
    if not is_valid_coordinates:
        return {
            "statusCode": 400,
            "headers": {
                "Access-Control-Allow-Headers": "*",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": "*",
            },
            "body": "Invalid coordinates, off grid!"
        }

    # persist
    grid = Grid(GRID_SIZE, x, y, direction)
    persist_state(table_name, grid)

    return {
        "statusCode": 200,
        "headers": {
            "Access-Control-Allow-Headers": "*",
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Methods": "*",
        },
        "body": grid.show()
    }
