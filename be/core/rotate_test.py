import unittest
from rotate import left, right


class RotateTest(unittest.TestCase):

    def test_left(self):
        self.assertEqual(left("SOUTH"), "EAST")
        self.assertEqual(left("EAST"), "NORTH")
        self.assertEqual(left("NORTH"), "WEST")
        self.assertEqual(left("WEST"), "SOUTH")

    def test_right(self):
        self.assertEqual(right("WEST"), "NORTH")
        self.assertEqual(right("NORTH"), "EAST")
        self.assertEqual(right("EAST"), "SOUTH")
        self.assertEqual(right("SOUTH"), "WEST")


if __name__ == '__main__':
    unittest.main()
