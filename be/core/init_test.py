import unittest
from grid import Grid
from config import GRID_SIZE
from init import parse_input


class InitTest(unittest.TestCase):

    def test_parse_input_example_a(self):
        grid = Grid(GRID_SIZE)
        parse_input("PLACE 0,0,NORTH", grid)
        parse_input("MOVE", grid)
        report = parse_input("REPORT", grid)
        self.assertEqual(report, "Output: 0,1,NORTH")

    def test_parse_input_example_b(self):
        grid = Grid(GRID_SIZE)
        parse_input("PLACE 0,0,NORTH", grid)
        parse_input("LEFT", grid)
        report = parse_input("REPORT", grid)
        self.assertEqual(report, "Output: 0,0,WEST")

    def test_parse_input_example_c(self):
        grid = Grid(GRID_SIZE)
        parse_input("PLACE 1,2,EAST", grid)
        parse_input("MOVE", grid)
        parse_input("MOVE", grid)
        parse_input("LEFT", grid)
        parse_input("MOVE", grid)
        report = parse_input("REPORT", grid)
        self.assertEqual(report, "Output: 3,3,NORTH")


if __name__ == '__main__':
    unittest.main()
