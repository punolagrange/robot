from config import DIRECTIONS, NORTH, WEST, SOUTH, EAST


def _left(direction_idx):
    return direction_idx + 1


def _right(direction_idx):
    return direction_idx - 1


def _rotate(direction, fn):
    assert direction in DIRECTIONS
    directions_order = [SOUTH, EAST, NORTH, WEST]
    directions_to_idx = {
        SOUTH: 0,
        EAST: 1,
        NORTH: 2,
        WEST: 3
    }
    direction_idx = directions_to_idx[direction]
    new_direction_idx = fn(direction_idx) % len(directions_order)
    return directions_order[new_direction_idx]


def left(direction):
    return _rotate(direction, _left)


def right(direction):
    return _rotate(direction, _right)
