from move import valid_coordinates


def place(x, y, direction):
    if not valid_coordinates(x, y):
        return False, None, None, None
    return True, x, y, direction
