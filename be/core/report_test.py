import unittest
from report import report


class ReportTest(unittest.TestCase):

    def test_report(self):
        self.assertEqual(report(1, 1, "SOUTH"), "Output: 1,1,SOUTH")


if __name__ == '__main__':
    unittest.main()
