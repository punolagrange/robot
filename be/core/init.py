import parse
import rotate
from grid import Grid
from config import GRID_SIZE
from move import move, valid_coordinates
from report import report


def parse_input(user_input: str, grid: Grid):
    user_input = user_input.strip(" ")
    place_cmd_called, x, y, direction = parse.place(user_input)

    if place_cmd_called:
        is_valid_coordinated = valid_coordinates(x, y)
        if is_valid_coordinated:
            grid.mutate(x, y, direction)
        else:
            return "Invalid coordinates!"
    if not grid.is_placed():
        return "Robot is not placed on the table!"

    if parse.move(user_input):
        moved, new_x, new_y = move(grid.x, grid.y, grid.direction)
        if moved:
            grid.mutate(new_x, new_y, grid.direction)
        else:
            return "The robot will fall off the table if it moves that direction. It therefore won't."
    elif parse.left(user_input):
        grid.mutate(grid.x, grid.y, rotate.left(grid.direction))
    elif parse.right(user_input):
        grid.mutate(grid.x, grid.y, rotate.right(grid.direction))
    elif parse.report(user_input):
        return report(grid.x, grid.y, grid.direction)


def run():
    grid = Grid(GRID_SIZE)
    while True:
        user_input = input("")
        res = parse_input(user_input, grid)
        if res is not None:
            print(res)


if __name__ == "__main__":
    run()
