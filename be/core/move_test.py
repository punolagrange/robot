import unittest
import move


class MoveTest(unittest.TestCase):

    def test_south(self):
        self.assertEqual(move._south(1, 1), (1, 0))

    def test_north(self):
        self.assertEqual(move._north(1, 1), (1, 2))

    def test_west(self):
        self.assertEqual(move._west(1, 1), (0, 1))

    def test_east(self):
        self.assertEqual(move._east(1, 1), (2, 1))

    def test_coordinate_is_valid(self):
        self.assertTrue(move._coordinate_is_valid(1, range(0, 3)))
        self.assertFalse(move._coordinate_is_valid(1, range(0, 1)))

    def test_move_helper(self):
        self.assertEqual(move._move(1, 1, 'SOUTH'), ((1, 0), 'SOUTH'))

    def test_move(self):
        self.assertEqual(move.move(1, 1, 'SOUTH'), (True, 1, 0))


if __name__ == '__main__':
    unittest.main()
