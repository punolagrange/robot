import unittest
from parse import move, report, left, right, place


class ParseTest(unittest.TestCase):

    def test_move(self):
        self.assertTrue(move("MOVE"))

    def test_report(self):
        self.assertTrue(report("REPORT"))

    def test_left(self):
        self.assertTrue(left("LEFT"))

    def test_right(self):
        self.assertTrue(right("RIGHT"))

    def test_place(self):
        placed, x, y, direction = place("PLACE 0,0,SOUTH")
        self.assertTrue(placed)
        self.assertEqual(x, 0)
        self.assertEqual(y, 0)
        self.assertEqual(direction, "SOUTH")

        self.assertFalse(place("PLACE a,0,SOUTH")[0])


if __name__ == '__main__':
    unittest.main()
