import re
from config import DIRECTIONS


def move(user_input):
    if user_input == "MOVE":
        return True
    return False


def report(user_input):
    if user_input == "REPORT":
        return True
    return False


def left(user_input):
    if user_input == "LEFT":
        return True
    return False


def right(user_input):
    if user_input == "RIGHT":
        return True
    return False


def place(user_input, directions=DIRECTIONS):
    m = re.match(
        r"(?P<place>PLACE) (?P<x>\d+),\s*(?P<y>\d+),\s*(?P<direction>.*)",
        user_input)

    if m:
        groups = m.groupdict()
        direction = groups['direction']
        if direction not in directions:
            print(
                "{0} is not a valid direction. Choose from: {1}".format(
                    direction, directions))
            return False, None, None, None
        return True, int(groups['x']), int(groups['y']), direction

    return False, None, None, None
