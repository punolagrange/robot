import unittest
from grid import Grid


class GridTest(unittest.TestCase):

    def test_init(self):
        my_grid = Grid(2)
        self.assertEqual(None, None)
        self.assertEqual(my_grid.x, None)
        self.assertEqual(my_grid.y, None)
        self.assertEqual(my_grid.direction, None)

    def test_mutate(self):
        my_grid = Grid(2)
        my_grid.mutate(1, 2, 'SOUTH')
        self.assertEqual(my_grid.x, 1)
        self.assertEqual(my_grid.y, 2)
        self.assertEqual(my_grid.direction, 'SOUTH')

    def test_reset(self):
        my_grid = Grid(2)
        my_grid.mutate(1, 2, 'SOUTH')
        my_grid.reset()
        self.assertEqual(my_grid.x, None)
        self.assertEqual(my_grid.y, None)
        self.assertEqual(my_grid.direction, None)


if __name__ == '__main__':
    unittest.main()
