import unittest
from place import place


class Place(unittest.TestCase):

    def test_place(self):
        self.assertEqual(place(1, 1, "SOUTH"), (True, 1, 1, 'SOUTH'))


if __name__ == '__main__':
    unittest.main()
