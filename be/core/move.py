from config import X_RANGE, Y_RANGE, \
    DIRECTIONS, NORTH, WEST, SOUTH, EAST


def _coordinate_is_valid(coordinate, range):
    if coordinate not in range:
        return False
    return True


def _x_valid_coordinate(x: int):
    return _coordinate_is_valid(x, X_RANGE)


def _y_valid_coordinate(y: int):
    return _coordinate_is_valid(y, Y_RANGE)


def valid_coordinates(x: int, y: int):
    return _x_valid_coordinate(x) and _y_valid_coordinate(y)


def _north(x: int, y: int):
    return x, y + 1


def _south(x: int, y: int):
    return x, y - 1


def _east(x: int, y: int):
    return x + 1, y


def _west(x: int, y: int):
    return x - 1, y


def _move(x: int, y: int, direction):
    assert direction in DIRECTIONS

    if direction == NORTH:
        return _north(x, y), direction
    elif direction == SOUTH:
        return _south(x, y), direction
    elif direction == WEST:
        return _west(x, y), direction
    elif direction == EAST:
        return _east(x, y), direction


def move(x, y, direction):
    assert direction in DIRECTIONS

    (new_x, new_y), _ = _move(x, y, direction)

    if not valid_coordinates(new_x, new_y):
        return False, None, None

    return True, new_x, new_y
