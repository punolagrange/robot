import json


class Grid:
    def __init__(self, size: int, x=None, y=None, direction=None) -> None:
        self.size: int = size
        self.x: int or None = x
        self.y: int or None = y
        self.direction: str or None = direction

    def __str__(self) -> str:
        return "({0}, {1}, {2})".format(self.x, self.y, self.direction)

    def mutate(self, x, y, direction) -> None:
        self.x = x
        self.y = y
        self.direction = direction

    def is_placed(self) -> bool:
        is_placed = self.x is not None and \
            self.y is not None and \
            self.direction is not None
        return is_placed

    def reset(self) -> None:
        self.x = None
        self.y = None
        self.direction = None

    def show(self):
        return json.dumps({
            "x": self.x,
            "y": self.y,
            "direction": self.direction
        })
