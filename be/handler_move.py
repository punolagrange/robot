import os
import json
from core.move import move
from persist import read_from_state, persist_state


def handler(_event, _context):
    table_name = os.environ["table_name"]

    # read state
    read_successfully, message, grid = read_from_state(table_name)
    if not read_successfully:
        return {
            "statusCode": 400,
            "headers": {
                "Access-Control-Allow-Headers": "*",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": "*",
            },
            "body": json.dumps(message)
        }

    # check if the move doesn't overflow - exit if can't
    moved, new_x, new_y = move(grid.x, grid.y, grid.direction)
    if not moved:
        return {
            "statusCode": 400,
            "headers": {
                "Access-Control-Allow-Headers": "*",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": "*",
            },
            "body": "The robot will fall off the table if it moves that direction. It therefore won't."
        }

    # persist move
    grid.mutate(new_x, new_y, grid.direction)
    persist_state(table_name, grid)

    return {
        "statusCode": 200,
        "headers": {
            "Access-Control-Allow-Headers": "*",
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Methods": "*",
        },
        "body": grid.show()
    }
