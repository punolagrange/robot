import os
from core.rotate import left
from persist import read_from_state, persist_state


def handler(_event, _context):
    table_name = os.environ["table_name"]
    _read_successfully, __message, grid = read_from_state(
        table_name,
        skip_placed_check=True)
    return {
        "statusCode": 200,
        "headers": {
            "Access-Control-Allow-Headers": "*",
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Methods": "*",
        },
        "body": grid.show()
    }
