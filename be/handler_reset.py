import os
from core.move import valid_coordinates
from core.grid import Grid
from core.config import GRID_SIZE
from persist import persist_state


def handler(_event, _context):
    table_name = os.environ["table_name"]
    grid = Grid(GRID_SIZE)
    grid.reset()
    persist_state(table_name, grid)

    return {
        "statusCode": 200,
        "headers": {
            "Access-Control-Allow-Headers": "*",
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Methods": "*",
        },
        "body": grid.show()
    }
