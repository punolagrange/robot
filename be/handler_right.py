import json
import os
from core.rotate import right
from persist import read_from_state, persist_state


def handler(_event, _context):
    table_name = os.environ["table_name"]
    read_successfully, message, grid = read_from_state(table_name)
    if not read_successfully:
        return {
            "statusCode": 400,
            "headers": {
                "Access-Control-Allow-Headers": "*",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": "*",
            },
            "body": json.dumps(message)
        }

    # turn left & persist
    grid.mutate(grid.x, grid.y, right(grid.direction))
    persist_state(table_name, grid)

    return {
        "statusCode": 200,
        "headers": {
            "Access-Control-Allow-Headers": "*",
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Methods": "*",
        },
        "body": grid.show()
    }
