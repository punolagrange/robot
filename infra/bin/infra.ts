#!/usr/bin/env node
import "source-map-support/register";
import * as cdk from "aws-cdk-lib";
import { MainStack } from "../lib/mainStack";
import { EnvTypes } from "../common/types/enums";
import { IEnvironment } from "../common/types/interfaces";
import { prodConfig } from "../config/environment/prod";

function generateMainStack(config: IEnvironment) {
  new MainStack(app, `SourcedStack-${config.env}`, {
    env: { account: config.account, region: config.region },
    account: config.account,
    region: config.region,
    repositoryTag: config.fe.repositoryTag,
    maxAzs: config.vpc.maxAzs,
    environment: config.env,
    certificateArn: config.fe.certificateArn,
    domainName: config.fe.domainName,
    subdomain: config.fe.subdomain,
  });
}

const app = new cdk.App();
const environment = app.node.tryGetContext("env");

if (environment === EnvTypes.dev) {
  // generateMainStack(devConfig);
} else if (environment === EnvTypes.prod) {
  generateMainStack(prodConfig);
} else {
  console.info(
    "env not passed or incorrectly passed. Please use -c env=<environment_type>"
  );
}
