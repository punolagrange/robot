import { Stack, StackProps, aws_apigateway, aws_ec2, aws_ecs, aws_ecr, aws_iam } from "aws-cdk-lib";
import { Construct } from "constructs";
import { EnvTypes } from "../common/types/enums";

interface Props extends StackProps {
  maxAzs: number;
  environment: EnvTypes;
  iamArnPrincipals: string[];
}
export default class CommonStack extends Stack {
  public readonly apiGw: aws_apigateway.RestApi;
  public readonly vpc: aws_ec2.Vpc;
  public readonly cluster: aws_ecs.Cluster;
  public readonly appEcr: aws_ecr.Repository;

  constructor(scope: Construct, id: string, props: Props) {
    super(scope, id, props);

    this.vpc = new aws_ec2.Vpc(this, 'Vpc', { 
      maxAzs: props.maxAzs, 
      enableDnsHostnames: true, 
      enableDnsSupport: true 
    });

    this.cluster = new aws_ecs.Cluster(this, 'Cluster', { vpc: this.vpc })

    this.apiGw = new aws_apigateway.RestApi(this, "RestApi", {
      deploy: true,
      retainDeployments: false,
      defaultCorsPreflightOptions: {
        allowHeaders: ["*"],
        allowCredentials: false,
        allowOrigins: ["*"],
      },
    });

    this.appEcr = new aws_ecr.Repository(this, 'AppEcr', {
      repositoryName: `robot-app-${props.environment}`,
    });
    props.iamArnPrincipals.forEach((iamArnPrincipal) => {
      this.appEcr.grantPull(new aws_iam.ArnPrincipal(iamArnPrincipal));
    });
  }
}
