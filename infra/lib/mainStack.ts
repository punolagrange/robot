import { Stack, StackProps } from "aws-cdk-lib";
import { Construct } from "constructs";
import CommonStack from "./commonStack";
import BeStack from "./beStack";
import FeStack from './feStack';
import { EnvTypes } from "../common/types/enums";

interface Props extends StackProps {
  repositoryTag: string;
  maxAzs: number;
  environment: EnvTypes;
  account: string;
  certificateArn: string;
  domainName: string;
  subdomain: string;
  region: string;
}
export class MainStack extends Stack {
  constructor(scope: Construct, id: string, props: Props) {
    super(scope, id, props);

    const common = new CommonStack(this, "Common", {
      env: { account: props.account, region: props.region },
      maxAzs: props.maxAzs,
      environment: props.environment,
      iamArnPrincipals: [`arn:aws:iam::${props.account}:root`],
    });
    new BeStack(this, "BE", {
      env: { account: props.account, region: props.region },
      apiGw: common.apiGw,
    });

    new FeStack(this, "FE", {
      env: { account: props.account, region: props.region },
      vpc: common.vpc,
      cluster: common.cluster,
      repositoryName: common.appEcr.repositoryName,
      repositoryTag: props.repositoryTag,
      certificateArn: props.certificateArn,
      domainName: props.domainName,
      subdomain: props.subdomain,
    })
  }
}
