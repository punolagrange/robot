import { Stack, StackProps, aws_ec2, aws_ecs, aws_ecr, aws_elasticloadbalancingv2, aws_route53, aws_route53_targets } from 'aws-cdk-lib';
import { TcpRetryEvent } from 'aws-cdk-lib/aws-appmesh';
import { Construct } from 'constructs';

interface Props extends StackProps {
  vpc: aws_ec2.IVpc;
  cluster: aws_ecs.ICluster;
  repositoryName: string;
  repositoryTag: string;
  certificateArn: string;
  domainName: string;
  subdomain: string;
}

export default class FeStack extends Stack {
  constructor(scope: Construct, id: string, props: Props) {
    super(scope, id, props);

    const lb = new aws_elasticloadbalancingv2.ApplicationLoadBalancer(this, 'LB', {
      vpc: props.vpc,
      internetFacing: true,
    });

    const defaultTg = new aws_elasticloadbalancingv2.ApplicationTargetGroup(this, 'DefaultTg', {
      vpc: props.vpc,
      port: 80,
    });
    lb.addListener('Listener', {
      open: true,
      defaultTargetGroups: [defaultTg],
      protocol: aws_elasticloadbalancingv2.ApplicationProtocol.HTTPS,
      certificates: [aws_elasticloadbalancingv2.ListenerCertificate.fromArn(props.certificateArn)]
    });

    const taskDefinition = new aws_ecs.FargateTaskDefinition(this, 'TaskDef');
    const container = taskDefinition.addContainer('robot', {
      image: aws_ecs.ContainerImage.fromEcrRepository(
        aws_ecr.Repository.fromRepositoryName(this, 'Repository', props.repositoryName),
        props.repositoryTag,
      ),
      memoryLimitMiB: 512,
      environment: {
      },
      logging: aws_ecs.LogDrivers.awsLogs({ streamPrefix: 'robot' }),
    });
    container.addPortMappings({
      containerPort: 80,
      protocol: aws_ecs.Protocol.TCP,
    });
    const service = new aws_ecs.FargateService(this, 'Service', {
      cluster: props.cluster,
      taskDefinition,
    });
    defaultTg.addTarget(service);

    const zone = aws_route53.HostedZone.fromLookup(this, 'ZoneB2B', {
      domainName: props.domainName,
    });
    const recordName = `${props.subdomain}.${props.domainName}`;
    new aws_route53.ARecord(this, 'SiteAliasRecord', {
      recordName: recordName,
      target: aws_route53.RecordTarget.fromAlias(new aws_route53_targets.LoadBalancerTarget(lb)),
      zone,
    });

  }
}
