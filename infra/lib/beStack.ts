import {
  Stack,
  StackProps,
  aws_lambda,
  aws_dynamodb,
  aws_apigateway,
} from "aws-cdk-lib";
import { Construct } from "constructs";
import * as path from "path";

interface Props extends StackProps {
  apiGw: aws_apigateway.RestApi;
}

export default class BeStack extends Stack {
  constructor(scope: Construct, id: string, props: Props) {
    super(scope, id, props);

    const table = new aws_dynamodb.Table(this, "Table", {
      partitionKey: { name: "id", type: aws_dynamodb.AttributeType.NUMBER },
      billingMode: aws_dynamodb.BillingMode.PAY_PER_REQUEST,
    });

    const leftFn = new aws_lambda.Function(this, "LeftFn", {
      runtime: aws_lambda.Runtime.PYTHON_3_8,
      handler: "handler_left.handler",
      code: aws_lambda.Code.fromAsset(path.join(__dirname, "../../be")),
      environment: {
        table_name: table.tableName,
      },
    });

    const rightFn = new aws_lambda.Function(this, "RightFn", {
      runtime: aws_lambda.Runtime.PYTHON_3_8,
      handler: "handler_right.handler",
      code: aws_lambda.Code.fromAsset(path.join(__dirname, "../../be")),
      environment: {
        table_name: table.tableName,
      },
    });

    const moveFn = new aws_lambda.Function(this, "MoveFn", {
      runtime: aws_lambda.Runtime.PYTHON_3_8,
      handler: "handler_move.handler",
      code: aws_lambda.Code.fromAsset(path.join(__dirname, "../../be")),
      environment: {
        table_name: table.tableName,
      },
    });

    const placeFn = new aws_lambda.Function(this, "PlaceFn", {
      runtime: aws_lambda.Runtime.PYTHON_3_8,
      handler: "handler_place.handler",
      code: aws_lambda.Code.fromAsset(path.join(__dirname, "../../be")),
      environment: {
        table_name: table.tableName,
      },
    });

    const reportFn = new aws_lambda.Function(this, "ReportFn", {
      runtime: aws_lambda.Runtime.PYTHON_3_8,
      handler: "handler_report.handler",
      code: aws_lambda.Code.fromAsset(path.join(__dirname, "../../be")),
      environment: {
        table_name: table.tableName,
      },
    });

    const resetFn = new aws_lambda.Function(this, "Reset", {
      runtime: aws_lambda.Runtime.PYTHON_3_8,
      handler: "handler_reset.handler",
      code: aws_lambda.Code.fromAsset(path.join(__dirname, "../../be")),
      environment: {
        table_name: table.tableName,
      },
    });

    table.grantReadData(reportFn);
    table.grantReadWriteData(resetFn);
    table.grantReadWriteData(leftFn);
    table.grantReadWriteData(rightFn);
    table.grantReadWriteData(moveFn);
    table.grantReadWriteData(placeFn);

    const items = props.apiGw.root;
    items
      .addResource("left")
      .addMethod("POST", new aws_apigateway.LambdaIntegration(leftFn));
    items
      .addResource("right")
      .addMethod("POST", new aws_apigateway.LambdaIntegration(rightFn));
    items
      .addResource("move")
      .addMethod("POST", new aws_apigateway.LambdaIntegration(moveFn));
    items
      .addResource("place")
      .addMethod("PUT", new aws_apigateway.LambdaIntegration(placeFn));
    items
      .addResource("report")
      .addMethod("GET", new aws_apigateway.LambdaIntegration(reportFn));
    items
      .addResource("reset")
      .addMethod("PUT", new aws_apigateway.LambdaIntegration(resetFn));
  }
}
