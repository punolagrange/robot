import { EnvTypes } from "../../common/types/enums";
import { IEnvironment } from "../../common/types/interfaces";

export const prodConfig: IEnvironment = {
  env: EnvTypes.prod,
  account: "884643815617",
  region: "ap-south-1",
  vpc: {
    maxAzs: 2,
  },
  fe: {
    repositoryTag: 'prod',
    certificateArn: 'arn:aws:acm:ap-south-1:884643815617:certificate/4d743dab-c505-479d-a427-36c2b7ccd7ff',
    domainName: 'go-demo.bambu.life',
    subdomain: 'sourcedrobot',
  }
};
