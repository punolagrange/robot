import { EnvTypes } from "../../common/types/enums";
import { IEnvironment } from "../../common/types/interfaces";

export const devConfig: IEnvironment = {
  env: EnvTypes.dev,
  account: "884643815617",
  region: "ap-northeast-1",
  vpc: {
    maxAzs: 2,
  },
  fe: {
    repositoryTag: 'dev',
    certificateArn: 'arn:aws:acm:ap-northeast-1:884643815617:certificate/f5b32088-8c46-40aa-a648-ccaf3c7c661a',
    domainName: 'go-test.bambu.life',
    subdomain: 'sourcedrobot',
  }
};
