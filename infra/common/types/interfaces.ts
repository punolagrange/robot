import { EnvTypes } from "./enums";

export interface IEnvironment {
  env: EnvTypes;
  account: string;
  region: string;
  vpc: {
    maxAzs: number;
  },
  fe: {
    repositoryTag: string;
    certificateArn: string;
    domainName: string;
    subdomain: string;
  }
}
