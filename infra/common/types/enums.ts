export enum EnvTypes {
  dev = "dev",
  prod = "prod",
  uat = "uat",
}
